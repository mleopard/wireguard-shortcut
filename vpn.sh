#!/bin/bash

case $1 in
	start)
		sudo systemctl start wg-quick@wg0;;
	stop)
		sudo systemctl stop wg-quick@wg0;;
	status)
		sudo systemctl status wg-quick@wg0;;
	*)
		echo "valid commands: start/stop/status";;
esac
