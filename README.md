# Wireguard vpn shortcut script

Helper script for wireguard VPN clients

## Install

```
sudo cp vpn.sh /usr/local/bin
```

## Usage

```
vpn start
vpn stop
vpn status
```
